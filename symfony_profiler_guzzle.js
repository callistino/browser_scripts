// ==UserScript==
// @name         Symfony Profiler Guzzle Tab
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://*.dev.peak10.com/_profiler/*?panel=guzzle
// @match        http://cam.peak10.com/_profiler/*?panel=guzzle
// @require      https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js
// @require      https://raw.githubusercontent.com/vkiryukhin/vkBeautify/master/vkbeautify.js
// @resource     css https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/darcula.min.css
// @grant        GM_addStyle
// @grant        GM_getResourceText
// ==/UserScript==

(function() {
    "use strict";
    
    // Add theme styles
    GM_addStyle(GM_getResourceText("css"));
    
    // Configure highlight.js
    hljs.configure({
        tabReplace: "  ",
        languages: ["apache", "http", "json", "xml", "sql"]
    });
    
    // Global code block styles
    let addCodeStyles = function(block) {
        block.style.maxWidth = "950px";
        block.style.whiteSpace = "pre-wrap";
    };
    
    let buttons = [].slice.call(document.querySelectorAll(".sf-tabs .sf-toggle"));
    
    document.addEventListener("click", event => {
        let button = event.target;
        
        if (
            buttons.indexOf(button) === -1 ||
            button.classList.contains("sf-toggle-off")
        ) {
            return;
        }
        
        button.parentNode.parentNode
        .querySelectorAll(".sf-toggle-content pre")
        .forEach(code => {
            // Already highlighted
            if (code.classList.contains("hljs")) {
                return;
            }
            
            // Decode url http
            try {
                code.textContent = decodeURIComponent(code.textContent);
            } catch (error) {}
            
            // General first highlight
            addCodeStyles(code);
            hljs.highlightBlock(code);
            
            // Format and re-highlight xml blocks
            code.querySelectorAll("span.xml").forEach(block => {
                block.textContent = vkbeautify.xml(block.textContent, 2);
                hljs.highlightBlock(block);
            });
            
            // Format and re-highlight json blocks
            code.querySelectorAll("span.json").forEach(block => {
                block.textContent = vkbeautify.json(block.textContent, 2);
                hljs.highlightBlock(block);
            });
            
            // Format and re-highlight soql blocks
            if (code.textContent.indexOf("SELECT") >= 0) {
                let text = code.textContent,
                query,
                label,
                soql;
                
                label = document.createElement("strong");
                label.textContent = "SOQL query:";
                
                query = vkbeautify.sql(
                    decodeURIComponent(
                        text.substring(text.indexOf("SELECT"), text.indexOf("HTTP/1.1"))
                    ),
                    2
                );
                soql = document.createElement("pre");
                soql.textContent = query;
                addCodeStyles(soql);
                
                code.parentNode.insertBefore(label, code.nextSibling);
                code.parentNode.insertBefore(soql, label.nextSibling);
                
                hljs.highlightBlock(soql);
            }
        });
    });
})();
